/*
 * Created by Daniel Marell 13-05-14 9:28 PM
 */
package com.caglabs.goldenzone.helloservice.impl;

import com.caglabs.goldenzone.common.BuildInfo;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.util.logging.Logger;

@Startup
@Singleton
public class StartupBean {
    private Logger logger = Logger.getLogger(StartupBean.class.getName());

    @PostConstruct
    void init() {
        logger.info("Application version: " + BuildInfo.getApplicationVersion());
        logger.info("Build tag: " + BuildInfo.getBuildTag());
        logger.info("Revision: " + BuildInfo.getRevision());
        logger.info("Build number: " + BuildInfo.getBuildNumber());
    }
}