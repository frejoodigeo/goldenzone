package com.caglabs.goldenzone.infomodel.dbloader;

import com.caglabs.goldenzone.infomodel.entity.GoldenzoneUser;
import com.caglabs.goldenzone.infomodel.entity.GoldenzoneUserDao;

public class ExampleInfoModelDBLoader extends AbstractDBLoaderBase {

    public void loadDB() {
        final GoldenzoneUserDao goldenzoneUserDao = new GoldenzoneUserDao();
        injectEntityManager(goldenzoneUserDao);
        withinTransaction(new Runnable() {
            @Override
            public void run() {
                goldenzoneUserDao.save(new GoldenzoneUser("kalle", "Kalle Banan"));
                goldenzoneUserDao.save(new GoldenzoneUser("guldrun", "Guldrun Zapp"));
                goldenzoneUserDao.save(new GoldenzoneUser("otto", "Otto von Apfelstrudel"));
            }
        });
    }

    public static void main(String[] args) throws DBLoadException {
        new ExampleInfoModelDBLoader().execute();
    }
}
